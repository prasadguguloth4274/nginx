server {
    listen 80;
    listen [::]:80;

    root /var/www/domain.com/html;
    index index.php index.html index.htm index.nginx-debian.html;

    server_name domain.com;

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }

    location / {
        try_files $uri/ $uri =404;
    }
}